using UnityEngine;

public class EnemyMovementMelee : EnemyMovement
{
    private float distanceFromPlayer = 1.5f;

    protected override void AttackMove()
    {
        base.AttackMove();

        Vector3 pos = transform.position + (characterEnemy.MoveSpeed * Time.deltaTime * (characterEnemy.PlayerDetected.transform.position - transform.position).normalized);
        if (Vector3.Distance(pos, characterEnemy.PlayerDetected.transform.position) > distanceFromPlayer)
            myRigidbody.MovePosition(pos);
        else
            characterEnemy.Attack();
    }
}