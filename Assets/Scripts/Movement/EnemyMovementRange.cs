using UnityEngine;

public class EnemyMovementRange : EnemyMovement
{
    private const float distanceToFollow = 6;
    private const float distanceToFlee = 5;

    protected override void AttackMove()
    {
        base.AttackMove();

        Vector3 pos = transform.position + characterEnemy.MoveSpeed * Time.deltaTime * (characterEnemy.PlayerDetected.transform.position - transform.position).normalized;
        if (Vector3.Distance(pos, characterEnemy.PlayerDetected.transform.position) <= distanceToFlee)
            myRigidbody.MovePosition(transform.position + characterEnemy.MoveSpeed * Time.deltaTime * (transform.position - characterEnemy.PlayerDetected.transform.position).normalized);
        else if (Vector3.Distance(pos, characterEnemy.PlayerDetected.transform.position) > distanceToFollow)
            myRigidbody.MovePosition(pos);

        characterEnemy.Attack();
    }
}
