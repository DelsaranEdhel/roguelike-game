using UnityEngine;

[RequireComponent(typeof(CharacterPlayer))]
public class PlayerMovement : Movement
{
    private CharacterPlayer characterPlayer;

    private Vector2 playerInput;
    private Vector3 mousePos;

    private Vector2 lastPos;
    public Vector2 Velocity { get; private set; }

    protected override void Awake()
    {
        base.Awake();

        characterPlayer = character as CharacterPlayer;
    }

    protected override void Update()
    {
        if(Camera.main != null)
        {
            mousePos = Input.mousePosition;
            mousePos.z = 0;
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);
        }

        animator.SetBool("isRunning", playerInput.magnitude > 0);

        FlipCharacterSide(mousePos);
    }

    protected override void FixedUpdate()
    {
        playerInput = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        myRigidbody.MovePosition(transform.position + (characterPlayer.MoveSpeed * Time.deltaTime * (Vector3) playerInput));

        Velocity = myRigidbody.position - lastPos;
        lastPos = myRigidbody.position;
    }

    public void SetPositionOnRoomEnter(Vector3 newPos)
    {
        transform.position = newPos;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.TryGetComponent(out Door door))
            door.GoThroughDoor();
    }
}
