using UnityEngine;

public abstract class Movement : MonoBehaviour
{
    protected Rigidbody2D myRigidbody;
    protected Animator animator;
    protected SpriteRenderer spriteRenderer;
    protected Character character;

    protected Transform weaponSlotTransform;

    protected virtual void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
        myRigidbody = GetComponent<Rigidbody2D>();
        character = GetComponent<Character>();

        weaponSlotTransform = transform.GetChild(0).transform;
    }

    protected virtual void Update() { }

    protected virtual void FixedUpdate() { }

    /// <summary>
    /// Flips character's sprite and equipped weapon based on given position
    /// </summary>
    /// <param name="basedPos">Position to which character will be flipped</param>
    protected void FlipCharacterSide(Vector3 basedPos)
    {
        if (basedPos.x > transform.position.x)
        {
            weaponSlotTransform.transform.localPosition = new Vector3(Mathf.Abs(weaponSlotTransform.localPosition.x), weaponSlotTransform.localPosition.y);
            spriteRenderer.flipX = false;
        }
        else if (basedPos.x < transform.position.x)
        {
            weaponSlotTransform.transform.localPosition = new Vector3(-Mathf.Abs(weaponSlotTransform.localPosition.x), weaponSlotTransform.localPosition.y);
            spriteRenderer.flipX = true;
        }
    }
}
