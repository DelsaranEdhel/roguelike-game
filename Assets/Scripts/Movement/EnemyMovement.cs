using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Extensions;

[RequireComponent(typeof(CharacterEnemy))]
public class EnemyMovement : Movement
{
    protected CharacterEnemy characterEnemy;
    protected IEnumerator idleMoveEnumerator;

    protected List<Vector3> idleMovePoints;

    protected override void Awake()
    {
        base.Awake();

        characterEnemy = character as CharacterEnemy;
    }

    /// <summary>
    /// Load up idle move points
    /// </summary>
    public virtual void LoadIdleMovePoints()
    {
        /*
         * we can't get move points in Awake, because after the enemy is being spawned we are moving his object to a different position,
         * so move points would be irrelevant
         */
        idleMovePoints = new List<Vector3>();
        foreach (Transform point in gameObject.FindObjectInChildWithTag(TagManager.idleMovePoints).transform)
            idleMovePoints.Add(point.position); //save move point position in WORLD position
    }

    protected override void FixedUpdate()
    {
        if (characterEnemy.IsCharacterAttacking())
            AttackMove();
    }

    public virtual void IdleMove()
    {
        idleMoveEnumerator = IdleMoveCoroutine();
        StartCoroutine(idleMoveEnumerator);

        animator.SetBool("isRunning", true);
    }

    protected virtual IEnumerator IdleMoveCoroutine()
    {
        Vector3 startPos = transform.position;
        Vector3 targetPos;

        do
        {
            targetPos = idleMovePoints[Random.Range(0, idleMovePoints.Count)];
        }
        while (startPos == targetPos);

        FlipCharacterSide(targetPos);

        //v = s / t
        //t = s / v

        float timeElapsed = 0;
        float lerpDuration = Vector3.Distance(startPos, targetPos) / characterEnemy.MoveSpeed;

        while(timeElapsed < lerpDuration)
        {
            //transform.position = Vector3.Lerp(startPos, targetPos, timeElapsed / lerpDuration);
            myRigidbody.MovePosition(Vector3.Lerp(startPos, targetPos, timeElapsed / lerpDuration));
            timeElapsed += Time.deltaTime;
            yield return null;
        }

        transform.position = targetPos;

        animator.SetBool("isRunning", false);
        yield return new WaitForSeconds(Random.Range(1, 3));

        IdleMove();






        //Ctrl + K + C
        //Ctrl + K + U
        //Vector3 startPos = transform.position;
        //Vector3 targetPos = transform.position + Vector3.up * 3;

        //float timeElapsed = 0;
        //float lerpDuration = 2;

        //while (timeElapsed < lerpDuration)
        //{
        //    transform.position = Vector3.Lerp(startPos, targetPos, timeElapsed / lerpDuration);
        //    timeElapsed += Time.deltaTime;
        //    yield return null;
        //}

        //timeElapsed = 0;
        //startPos = transform.position;
        //targetPos = transform.position + Vector3.down * 3;
        //while (timeElapsed < lerpDuration)
        //{
        //    transform.position = Vector3.Lerp(startPos, targetPos, timeElapsed / lerpDuration);
        //    timeElapsed += Time.deltaTime;
        //    yield return null;
        //}

        //transform.position = targetPos;

        //IdleMove();

    }

    protected virtual void AttackMove() 
    {
        if(idleMoveEnumerator != null)
        {
            StopCoroutine(idleMoveEnumerator);
            animator.SetBool("isRunning", false);
            idleMoveEnumerator = null;
        }

        FlipCharacterSide(characterEnemy.PlayerDetected.transform.position);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        //IMPORTANT - dealt damage is DamageRate from CharacterInfo - not weapon damage
        if (collision.gameObject.layer == LayerMask.NameToLayer(LayerMaskManager.EnemyTouch_LM))
            collision.transform.parent.GetComponent<IDamageable>().Damage(characterEnemy.DamageRate);
    }
}
