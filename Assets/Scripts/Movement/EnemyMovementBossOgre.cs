using System.Collections;
using UnityEngine;

public class EnemyMovementBossOgre : EnemyMovement
{
    private float nextChargeTime = 2f;
    private Vector3 chargeDirection;

    /// <summary>
    /// CHARGING - enemy is currently moving (charging)
    /// HIT_WALL - enemy has hit the wall (stop moving and wait some time)
    /// WAITING - enemy is waiting for next state, which will be CHARGING (set direction and set to CHARGING)
    /// </summary>
    private enum ActionState { CHARGING, HIT_WALL, WAITING }
    private ActionState attackState;

    protected override void Awake()
    {
        base.Awake();
        attackState = ActionState.WAITING;
    }

    public override void LoadIdleMovePoints() { }

    public override void IdleMove() { }

    public void DecreaseNextChargeTime() =>
        nextChargeTime /= 2;

    protected override void AttackMove()
    {
        switch (attackState)
        {
            case ActionState.WAITING:
                attackState = ActionState.CHARGING;
                chargeDirection = characterEnemy.PlayerDetected.transform.position - transform.position;
                FlipCharacterSide(characterEnemy.PlayerDetected.transform.position);
                break;
            case ActionState.CHARGING:
                myRigidbody.MovePosition(transform.position + characterEnemy.MoveSpeed * Time.deltaTime * chargeDirection.normalized);
                break;
            default:
                break;
        }
    }

    private IEnumerator ChargeWait()
    {
        attackState = ActionState.HIT_WALL;
        yield return new WaitForSeconds(nextChargeTime);
        attackState = ActionState.WAITING;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.gameObject.layer == LayerMask.NameToLayer(LayerMaskManager.Wall_LM))
        {
            animator.SetBool("isRunning", false);
            StartCoroutine(ChargeWait());
        }
    }
}
