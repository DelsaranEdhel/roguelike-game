using System.Collections;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private const float destroyTime = 5f;

    private float damage;
    private int targetLayerMasks;

    public void Shoot(ItemInfoWeaponRanged itemInfoWeapon, Character character, int targetLayerMasks, Vector2 direction)
    {
        this.targetLayerMasks = targetLayerMasks;

        damage = itemInfoWeapon.damage * character.DamageRate;

        GetComponent<Rigidbody2D>().velocity = 100 * itemInfoWeapon.arrowSpeed * Time.deltaTime * direction.normalized;
        StartCoroutine(DestroyAfterTime(destroyTime));
    }

    private IEnumerator DestroyAfterTime(float time)
    {
        yield return new WaitForSeconds(time);

        BulletsPool.bulletsPool.Release(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //detects if detected GameObject is in one of the target layers
        if (collision.TryGetComponent(out IDamageable damageable) && (targetLayerMasks & (1 << collision.gameObject.layer)) > 0)
        {
            damageable.Damage(damage);
            StopAllCoroutines();
            StartCoroutine(DestroyAfterTime(0f));
        }
    }
}
