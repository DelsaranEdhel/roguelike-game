using UnityEngine;
using UnityEngine.Pool;

public class BulletsPool : MonoBehaviour
{
    [SerializeField]
    private GameObject arrow;

    public static IObjectPool<GameObject> bulletsPool;

    private void Awake()
    {
        bulletsPool = new ObjectPool<GameObject>(
            createFunc: () => Instantiate(arrow, transform),
            actionOnGet: (obj) => obj.SetActive(true),
            actionOnRelease: (obj) => obj.SetActive(false),
            actionOnDestroy: (obj) => Destroy(obj),
            collectionCheck: false,
            defaultCapacity: 20,
            maxSize: 25
            );
    }
}
