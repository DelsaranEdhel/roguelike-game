using UnityEngine;

namespace Extensions
{
    public static class ObjectFindExtension
    {
        public static GameObject FindObjectInChildWithTag(this GameObject parent, string tag)
        {
            Transform transform = parent.transform;

            foreach (Transform t in transform)
            {
                if (t.CompareTag(tag))
                    return t.gameObject;
            }

            return null;
        }
    }
}
