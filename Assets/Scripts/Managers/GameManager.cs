using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    public static GameManager Instance { get { return _instance; } }

    public const int MainMenuScene = 0;
    public const int GameScene = 1;

    private bool isGamePaused = false;
    private bool hasGameEnded = false;

    private GameUI gameUI;
    public GameUI GameUI
    {
        get
        {
            if(!gameUI)
                gameUI = FindObjectOfType<GameUI>();

            if (!gameUI)
                throw new System.Exception("GameUI object was not found!");

            return gameUI;
        }
        set { gameUI = value; }
    }

    public CharacterPlayer PlayerCharacter { get; private set; }

    private void Awake()
    {
        //defining singleton
        if (Instance != null && Instance != this)
            Destroy(this.gameObject);
        else
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }

        PlayerCharacter = FindObjectOfType<CharacterPlayer>();

        Application.targetFrameRate = 60;
    }

    private void Start()
    {
        //Used only for testing, when starting a game from Game Scene
        if (SceneManager.GetActiveScene().buildIndex == GameScene)
            RoomManager.Instance.StartLevel();
    }

    private void Update()
    {
        if (!hasGameEnded && SceneManager.GetActiveScene().buildIndex == GameScene &&
            (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P)))
            PauseGame();

        if (Input.GetKeyDown(KeyCode.Tab) || Input.GetKeyDown(KeyCode.M))
            Minimap.ShowMinimap();
    }

    public void PauseGame()
    {
        isGamePaused = !isGamePaused;

        if (isGamePaused)
        {
            GameUI.ShowPausePanel(true);
            Time.timeScale = 0;
        }
        else
        {
            GameUI.ShowPausePanel(false);   
            Time.timeScale = 1;
        }
    }

    public void EndGame()
    {
        GameUI.ShowYouWonPanel();

        Time.timeScale = 0;
        hasGameEnded = true;
    }

    private void StartGame(Scene scene, LoadSceneMode mode)
    {
        ItemsRandomizer.Initialize();
        RoomManager.Instance.StartLevel();

        PlayerCharacter = FindObjectOfType<CharacterPlayer>();

        AudioManager.Instance.PlayGameMusic();
    }

    public void MoveToScene(int sceneIndex)
    {
        SceneManager.sceneLoaded -= StartGame; 

        if(sceneIndex == MainMenuScene)
        {
            Time.timeScale = 1;
            hasGameEnded = false;
        }

        switch(sceneIndex)
        {
            case MainMenuScene:
                Time.timeScale = 1;
                hasGameEnded = false;
                //clear STATIC events when leaving a game scene
                Item.pickUpEvent = null;
                HealthBar.updateHealthEvent = null;
                break;
            case GameScene:
                SceneManager.sceneLoaded += StartGame;
                break;
        }

        SceneManager.LoadScene(sceneIndex, LoadSceneMode.Single);
    }

    public void PlayerDeath()
    {
        hasGameEnded = true;
        GameUI.ShowDeathPanel();
    }
}
