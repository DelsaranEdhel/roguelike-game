using UnityEngine;

public class LayerMaskManager : MonoBehaviour
{
    public const string Enemy_LM = "Enemy";
    public const string Player_LM = "Player";
    public const string Wall_LM = "Wall";
    public const string EnemyTouch_LM = "EnemyTouch";
}
