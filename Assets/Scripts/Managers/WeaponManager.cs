using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    private static WeaponManager _instance;
    public static WeaponManager Instance { get { return _instance; } }

    private void Awake()
    {
        //defining singleton
        if (Instance != null && Instance != this)
            Destroy(this.gameObject);
        else
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }
    }

    /// <summary>
    /// Spawns an arrow from ranged attack
    /// </summary>
    /// <param name="itemInfo">ItemInfo of item which was used</param>
    /// <param name="targetLayerMasks">LayerMasks to be damaged</param>
    /// <param name="spawnPos">Start position of arrow object</param>
    /// <param name="direction">Direction of arrow object</param>
    public void ShootArrow(ItemInfoWeaponRanged itemInfo, Character character, int targetLayerMasks, Vector3 spawnPos, Vector2 direction)
    {
        GameObject arrow = BulletsPool.bulletsPool.Get();
        arrow.transform.position = spawnPos;
        arrow.GetComponent<SpriteRenderer>().sprite = itemInfo.arrowSprite;
        arrow.transform.up = direction.normalized;
        arrow.GetComponentInParent<Bullet>().Shoot(itemInfo, character, targetLayerMasks, direction);
    }

    /// <summary>
    /// Performs an melee attack
    /// </summary>
    /// <param name="itemInfo">ItemInfo of item which was used</param>
    /// <param name="startPos">Start position of OverlapCircle</param>
    /// <param name="targetLayerMasks">LayerMasks to be damaged</param>
    public void MeleeSlash(ItemInfoWeaponMelee itemInfo, Character character, Vector3 startPos, int targetLayerMasks)
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(startPos, itemInfo.damageRadius, targetLayerMasks);
        foreach (Collider2D collider in colliders)
        {
            if (collider.TryGetComponent(out IDamageable damageable))
            {
                damageable.Damage(itemInfo.damage * character.DamageRate);

                Debug.Log("Attacked character: " + collider.gameObject.name);
            }
        }
    }
}
