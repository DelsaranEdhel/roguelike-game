using UnityEngine;

public class AudioManager : MonoBehaviour
{
    private static AudioManager _instance;
    public static AudioManager Instance { get { return _instance; } }

    public const string GameMusic = "GameMusic";
    public const string BossMusic = "BossMusic";
    public const string MainMenuMusic = "MainMenuMusic";

    public const string ClickUISound = "ClickUISound";
    public const string GoblinDeathSound = "GoblinDeathSound";

    private void Awake()
    {
        //defining singleton
        if (Instance != null && Instance != this)
            Destroy(this.gameObject);
        else
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }
    }

    public void PlaySound(string soundName) =>
        transform.Find(soundName).GetComponent<AudioSource>().Play();

    public void StopSound(string soundName) =>
        transform.Find(soundName).GetComponent<AudioSource>().Stop();

    public void PlayBossMusic()
    {
        StopSound(MainMenuMusic);
        StopSound(GameMusic);
        PlaySound(BossMusic);
    }

    public void PlayGameMusic()
    {
        StopSound(MainMenuMusic);
        StopSound(BossMusic);
        PlaySound(GameMusic);
    }

    public void PlayMainMenuMusic()
    {
        StopSound(GameMusic);
        StopSound(BossMusic);

        transform.Find(MainMenuMusic).GetComponent<AudioSource>().volume = 0.5f;
        PlaySound(MainMenuMusic);
    }

    public AudioSource GetAudioSource(string name) =>
        transform.Find(name).GetComponent<AudioSource>();
}
