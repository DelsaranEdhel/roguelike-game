using UnityEngine;

[RequireComponent(typeof(EnemyMovement))]
public class CharacterEnemy : Character
{
    public override float Health 
    { 
        get => base.Health;
        set
        {
            if (value < base.Health && state.Equals(State.Idle))
            {
                PlayerDetected = GameManager.Instance.PlayerCharacter;
                state = State.Attack;
            }

            base.Health = value;
        }
    }

    protected EnemyMovement enemyMovement;
    public CharacterPlayer PlayerDetected { get; protected set; }

    [SerializeField]
    protected float detectRadius;

    [SerializeField]
    private bool drawGizmo = false;

    protected enum State { Idle, Attack }
    protected State state;

    protected override void Awake()
    {
        base.Awake();

        TargetAttackLayerMasks = LayerMask.GetMask(new string[] { LayerMaskManager.Player_LM });
        enemyMovement = GetComponent<EnemyMovement>();
        state = State.Idle;
        equippedWeapon = weaponMeleePos.GetComponentInChildren<Item>() as ItemWeapon;
    }

    protected override void Start()
    {
        base.Start();

        equippedWeapon.PickUpItem(this);
    }

    /// <summary>
    /// Executes when enemy is being activated (when player has entered the room)
    /// </summary>
    public virtual void Activate()
    {
        enemyMovement.LoadIdleMovePoints();
        gameObject.SetActive(true);
        enemyMovement.IdleMove();
    }

    protected override void Update()
    {
        if (state == State.Idle)
            IdleAction();
        else
            AttackAction();
    }

    protected virtual void IdleAction()
    {
        RaycastHit2D hit = Physics2D.CircleCast(transform.position, detectRadius, Vector2.zero, 0, LayerMask.GetMask(new string[] {LayerMaskManager.Player_LM}));
        if (hit.collider)
        {
            PlayerDetected = hit.collider.GetComponent<CharacterPlayer>();
            state = State.Attack;
            Debug.Log("<color='cyan'> " + gameObject.name + " has detected a player! </color>");
        }

        //when enemy has a ranged weapon then face a weapon to his move direction
        if(equippedWeapon && equippedWeapon is ItemWeaponRanged)
        {
            if (spriteRenderer.flipX)
                equippedWeapon?.FaceWeaponTo(transform.position + Vector3.left);
            else
                equippedWeapon?.FaceWeaponTo(transform.position + Vector3.right);
        }
    }

    protected virtual void AttackAction() =>
        equippedWeapon?.FaceWeaponTo(PlayerDetected.transform.position);

    public void Attack() => PerformAttack();

    public bool IsCharacterAttacking() => state == State.Attack;

    protected override void CharacterDeath()
    {
        AudioManager.Instance.PlaySound(AudioManager.GoblinDeathSound);
        RoomManager.Instance.enemyKilled(this);
        base.CharacterDeath();
    }

    protected void OnDrawGizmos()
    {
        if(drawGizmo)
            Gizmos.DrawWireSphere(transform.position, detectRadius);
    }
}
