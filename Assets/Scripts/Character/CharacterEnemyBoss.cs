using UnityEngine;

public class CharacterEnemyBoss : CharacterEnemy
{
    public override float Health
    { 
        get => base.Health;
        set
        {
            base.Health = value;

            bossHealthBar.UpdateHealth(MaxHealth, Health);
        }
    }

    private BossHealthBar bossHealthBar;

    protected override void Awake()
    {
        bossHealthBar = GetComponentInChildren<Canvas>().GetComponent<BossHealthBar>();
        base.Awake();
    }

    public override void Activate()
    {
        bossHealthBar.EnableHealthBar(characterInfo.characterName);
        base.Activate();
    }

    protected override void CharacterDeath()
    {
        bossHealthBar.DisableHealthBar();
        base.CharacterDeath();
    }
}
