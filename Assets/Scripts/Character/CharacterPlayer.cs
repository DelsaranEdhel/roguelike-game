using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerMovement))]
public class CharacterPlayer : Character
{
    public override float Health 
    { 
        get => base.Health; 
        set
        {
            if (isInvulnerable)
                return;

            base.Health = value;

            if (Health > MaxHealth)
                Health = MaxHealth;
            else if(Health < 0)
                Health = 0;

            StartCoroutine(InvulnerableCoroutine());
            HealthBar.updateHealthEvent(MaxHealth, Health);
        }
    }

    private List<ItemInfoBooster> equippedBoosters = new List<ItemInfoBooster>();

    //make player invulnerable to any damage for some amount of time
    private bool isInvulnerable;

    protected override void Awake()
    {
        base.Awake();

        TargetAttackLayerMasks = LayerMask.GetMask(new string[] { LayerMaskManager.Enemy_LM });
        Item.pickUpEvent += ItemPickUp;
    }

    protected override void Update()
    {
        base.Update();

        if (Input.GetKeyDown(KeyCode.Mouse0))
            PerformAttack();
        else if (Input.GetKeyDown(KeyCode.Q))
            DropWeapon();

        equippedWeapon?.WeaponMouseFollow();
    }

    private bool ItemPickUp(Item pickedItem)
    {
        switch (pickedItem)
        {
            case ItemWeapon itemWeapon:
                if (equippedWeapon == null)
                {
                    itemWeapon.transform.SetParent(weaponMeleePos);
                    itemWeapon.transform.position = weaponMeleePos.position;
                    equippedWeapon = itemWeapon;
                    return true;
                }
                break;
            case ItemBooster itemBooster:
                UpdateModifiers(itemBooster);
                return true;
        }

        return false;
    }

    private void UpdateModifiers(ItemBooster itemBooster)
    {
        equippedBoosters.Add(itemBooster.itemInfoBooster);
        Health += itemBooster.itemInfoBooster.healthRestore;
        MaxHealth += itemBooster.itemInfoBooster.healthIncrease;
        MoveSpeed += itemBooster.itemInfoBooster.moveSpeedIncrease;
        ArmorRate += itemBooster.itemInfoBooster.armorRateIncrease;
        DamageRate += itemBooster.itemInfoBooster.damageRateIncrease;

        //update health bar
        Health = Health;
    }

    private void DropWeapon()
    {
        if (!equippedWeapon)
            return;

        equippedWeapon.DropWeapon();
        equippedWeapon = null;
    }

    protected override void CharacterDeath()
    {
        base.CharacterDeath();
        
        GameManager.Instance.PlayerDeath();
    }

    private IEnumerator InvulnerableCoroutine()
    {
        isInvulnerable = true;
        yield return new WaitForSeconds(0.5f);
        isInvulnerable = false;
    }

    public PlayerMovement GetPlayerMovement() =>
        GetComponent<PlayerMovement>();
}
