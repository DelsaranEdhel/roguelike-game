using UnityEngine;

public class CharacterEnemyRange : CharacterEnemy
{
    private float arrowSpeed;

    protected override void Start()
    {
        base.Start();
        arrowSpeed = (equippedWeapon as ItemWeaponRanged).GetArrowSpeed();
    }

    protected override void AttackAction()
    {
        /*
        //Vector3 direction = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")).normalized;
        Vector3 direction = (Vector2)PlayerDetected.transform.position + PlayerMovement.trackVelocity;
        float distance = Vector3.Distance(transform.position, PlayerDetected.transform.position);
        float travelTime = distance / (100 * 10 * Time.deltaTime * equippedWeapon.transform.right).magnitude;
        Vector3 offset = PlayerDetected.transform.position + direction * travelTime;

        equippedWeapon?.FaceWeaponTo(PlayerDetected.transform.position + offset);
        */

        Vector3 finalPos = PredictPosition(
            PlayerDetected.transform.position,
            transform.position,
            PlayerDetected.GetPlayerMovement().Velocity * 10,
            (100 * arrowSpeed * Time.deltaTime * equippedWeapon.transform.right).magnitude
            );
        equippedWeapon?.FaceWeaponTo(finalPos);
    }

    private Vector3 PredictPosition(Vector3 targetPosition, Vector3 shooterPosition, Vector3 targetVelocity, float projectileSpeed)
    {
        Vector3 distance = targetPosition - shooterPosition;
        float targetMoveAngle = Vector3.Angle(-distance, targetVelocity) * Mathf.Deg2Rad;

        //if target has stopped moving OR if the projectile can not catch up with the target
        if (targetVelocity.magnitude == 0 || targetVelocity.magnitude > projectileSpeed && Mathf.Sin(targetMoveAngle) / projectileSpeed > Mathf.Cos(targetMoveAngle) / targetVelocity.magnitude)
            return targetPosition;

        float shootAngle = Mathf.Asin(Mathf.Sin(targetMoveAngle) * targetVelocity.magnitude / projectileSpeed);
        return targetPosition + targetVelocity * distance.magnitude / Mathf.Sin(Mathf.PI - targetMoveAngle - shootAngle) * Mathf.Sin(shootAngle) / targetVelocity.magnitude;
    }
}
