using System.Collections;
using UnityEngine;

public abstract class Character : MonoBehaviour, IDamageable
{
    public virtual float Health { get; set; }

    [SerializeField]
    protected CharacterInfo characterInfo;
    public CharacterInfo CharacterInfo
    {
        get { return characterInfo; }
        protected set { characterInfo = value; }
    }

    public float MaxHealth { get; protected set; }
    public float MoveSpeed { get; protected set; }
    public float ArmorRate { get; protected set; }
    public float DamageRate { get; protected set; }

    protected Transform weaponMeleePos;
    protected ItemWeapon equippedWeapon;

    protected SpriteRenderer spriteRenderer;
    protected AudioSource hurtSound;

    public int TargetAttackLayerMasks { get; protected set; }

    protected Material defaultSpriteRendererMaterial;
    protected IEnumerator damageEffectEnumerator;

    protected virtual void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        weaponMeleePos = transform.GetChild(0);
        defaultSpriteRendererMaterial = spriteRenderer.material;
        hurtSound = GetComponent<AudioSource>();

        MaxHealth = characterInfo.maxHealth;
        MoveSpeed = characterInfo.moveSpeed;
        ArmorRate = characterInfo.armorRate;
        DamageRate = characterInfo.damageRate;
    }

    protected virtual void Start()
    {
        Health = MaxHealth;
    }

    protected virtual void Update() { }

    protected void PerformAttack() => equippedWeapon?.Attack();

    public void Damage(float damage)
    {
        damage /= ArmorRate;

        Health -= damage;
        DamageSound();

        //if damage effect is still running then stop it
        if(damageEffectEnumerator != null)
        {
            StopCoroutine(damageEffectEnumerator);
            spriteRenderer.material = defaultSpriteRendererMaterial;
        }

        damageEffectEnumerator = DamageEffect();
        StartCoroutine(damageEffectEnumerator);

        if (Health <= 0)
            CharacterDeath();
    }

    protected virtual void CharacterDeath() => gameObject.SetActive(false);

    /// <summary>
    /// Creates a flash effect when taking damage (changes material to make sprite completely white and then goes back to default material)
    /// </summary>
    /// <returns></returns>
    protected virtual IEnumerator DamageEffect()
    {
        spriteRenderer.material = Resources.Load<Material>("Materials/DamageMaterial");
        yield return new WaitForSeconds(0.1f);
        spriteRenderer.material = defaultSpriteRendererMaterial;
    }

    protected void DamageSound()
    {
        hurtSound.Play();
    }
}
