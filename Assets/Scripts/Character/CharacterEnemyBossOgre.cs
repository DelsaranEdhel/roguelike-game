public class CharacterEnemyBossOgre : CharacterEnemyBoss
{
    private bool isSecondPhase = false;
    private bool isThirdPhase = false;

    public override float Health 
    { 
        get => base.Health;
        set
        {
            base.Health = value;

            if (!isSecondPhase && Health <= MaxHealth / 3)
            {
                MoveSpeed *= 2f;
                GetEnemyMovement().DecreaseNextChargeTime();
                isSecondPhase = true;
            }
            else if(!isThirdPhase && Health <= (MaxHealth / 3) * 2)
            {
                MoveSpeed *= 2f;
                GetEnemyMovement().DecreaseNextChargeTime();
                isThirdPhase = true;
            }
        }
    }

    private EnemyMovementBossOgre GetEnemyMovement() =>
        enemyMovement as EnemyMovementBossOgre;
}
