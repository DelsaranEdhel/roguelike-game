using UnityEngine;

[CreateAssetMenu(fileName = "Character", menuName = "Characters/Character")]
public class CharacterInfo : ScriptableObject
{
    public string characterName;
    public Sprite characterSprite;
    public float maxHealth;
    public float moveSpeed;
    public float armorRate;
    public float damageRate;

    //moze dodac przechowywanie Animation lub Animator
}
