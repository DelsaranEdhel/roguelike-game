using UnityEngine;

[CreateAssetMenu(fileName = "Ranged Weapon", menuName = "Items/Item Ranged Weapon")]
public class ItemInfoWeaponRanged : ItemInfoWeapon
{
    public Sprite arrowSprite;
    public float arrowSpeed;
}
