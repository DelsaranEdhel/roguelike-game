using UnityEngine;

[CreateAssetMenu(fileName = "Melee Weapon", menuName = "Items/Item Melee Weapon")]
public class ItemInfoWeaponMelee : ItemInfoWeapon
{
    public float damageRadius;
}
