using UnityEngine;

[CreateAssetMenu(fileName = "Booster", menuName = "Items/Booster")]
public class ItemInfoBooster : ItemInfo
{
    public float healthRestore;
    public float healthIncrease;
    public float moveSpeedIncrease;
    public float armorRateIncrease;
    public float damageRateIncrease;
}
