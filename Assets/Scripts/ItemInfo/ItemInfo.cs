using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "Items/Item")]
public class ItemInfo : ScriptableObject
{
    public string itemName;
    public Sprite sprite;
    public float value;
}
