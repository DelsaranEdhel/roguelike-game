using System;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public static Action<float, float> updateHealthEvent;

    private Image fillHealthBar;

    private void Awake()
    {
        fillHealthBar = GetComponent<Image>();

        updateHealthEvent += UpdateHealth;
    }

    private void UpdateHealth(float maxHealth, float currentHealth)
    {
        fillHealthBar.fillAmount = currentHealth / maxHealth;
    }
}
