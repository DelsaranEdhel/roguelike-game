using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    private static bool isSoundMuted = false;

    [SerializeField]
    private GameObject howToPlayPanel;
    [SerializeField]
    private GameObject optionsPanel;

    [SerializeField]
    private Image transitionImage;

    [SerializeField]
    private Sprite soundOnSprite;
    [SerializeField]
    private Sprite soundOffSprite;

    /*
     * 
     * Main Menu
     * 
     */
    
    public void PlayOnClick()
    {
        AudioManager.Instance.PlaySound(AudioManager.ClickUISound);
        StartCoroutine(LoadGame());
    }

    private IEnumerator LoadGame()
    {
        Color32 color = transitionImage.color;
        color.a = 255;
        transitionImage.color = color;
        transitionImage.canvasRenderer.SetAlpha(0);
        transitionImage.CrossFadeAlpha(1, 2f, false);

        AudioSource mainMenuMusic = AudioManager.Instance.GetAudioSource(AudioManager.MainMenuMusic);

        float timeElapsed = 0;
        float lerpDuration = 2f;
        while(timeElapsed < lerpDuration)
        {
            mainMenuMusic.volume = Mathf.Lerp(0.5f, 0, timeElapsed / lerpDuration);
            timeElapsed += Time.deltaTime;
            yield return null;
        }

        GameManager.Instance.MoveToScene(GameManager.GameScene);
    }

    public void HowToPlayOnClick()
    {
        AudioManager.Instance.PlaySound(AudioManager.ClickUISound);
        howToPlayPanel.SetActive(true);
    }

    public void OptionsOnClick()
    {
        AudioManager.Instance.PlaySound(AudioManager.ClickUISound);
        optionsPanel.SetActive(true);
    }

    public void ExitOnClick()
    {
        AudioManager.Instance.PlaySound(AudioManager.ClickUISound);
        Application.Quit();
    }


    /*
     * 
     * Options
     * 
     */

    /// <summary>
    /// OnClick method which enables in-game sound
    /// </summary>
    /// <param name="image">Image component of a sound icon</param>
    public void SoundOnClick(Image image)
    {
        isSoundMuted = !isSoundMuted;
     
        if(isSoundMuted)
        {
            AudioListener.volume = 0;
            image.sprite = soundOffSprite;
        }
        else
        {
            AudioListener.volume = 1;
            image.sprite = soundOnSprite;
        }
    }

    public void BackOnClick()
    {
        AudioManager.Instance.PlaySound(AudioManager.ClickUISound);
        howToPlayPanel.SetActive(false);
        optionsPanel.SetActive(false);
    }
}
