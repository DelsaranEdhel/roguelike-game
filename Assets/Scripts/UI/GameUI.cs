using UnityEngine;

public class GameUI : MonoBehaviour
{
    private GameObject pausePanel;
    private GameObject deathPanel;
    private GameObject youWonPanel;

    private void Awake()
    {
        pausePanel = transform.GetChild(0).gameObject;
        deathPanel = transform.GetChild(1).gameObject;
        youWonPanel = transform.GetChild(2).gameObject;
    }

    public void ShowPausePanel(bool show)
    {
        pausePanel.SetActive(show);
    }

    public void ShowDeathPanel()
    {
        deathPanel.SetActive(true);
    }

    public void ShowYouWonPanel()
    {
        youWonPanel.SetActive(true);
    }

    public void ResumeGameOnClick()
    {
        AudioManager.Instance.PlaySound(AudioManager.ClickUISound);
        GameManager.Instance.PauseGame();
    }

    public void MainMenuOnClick()
    {
        AudioManager.Instance.PlaySound(AudioManager.ClickUISound);
        AudioManager.Instance.PlayMainMenuMusic();
        GameManager.Instance.MoveToScene(GameManager.MainMenuScene);
    }
}
