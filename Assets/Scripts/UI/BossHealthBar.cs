using System;
using UnityEngine;
using UnityEngine.UI;

public class BossHealthBar : MonoBehaviour
{
    [SerializeField]
    private GameObject healthBar;
    [SerializeField]
    private Image fillHealthBar;
    [SerializeField]
    private Text textHealthBar;

    public void UpdateHealth(float maxHealth, float currentHealth)
    {
        fillHealthBar.fillAmount = currentHealth / maxHealth;

        if (currentHealth <= 0)
            DisableHealthBar();
    }

    public void EnableHealthBar(string bossName)
    {
        textHealthBar.text = bossName;
        healthBar.SetActive(true);
    }

    public void DisableHealthBar()
    {
        textHealthBar.text = "";
        healthBar.SetActive(false);
    }
}
