using UnityEngine;
using UnityEngine.UI;

public class MinimapRoom : MonoBehaviour
{
    [SerializeField]
    private Image mainRoomImage;
    [SerializeField]
    private Transform imagesTransform;
    [SerializeField]
    private Transform specialIconsTransform;

    public void ShowRoom(bool set) =>
        imagesTransform.gameObject.SetActive(set);

    public void ShowIcons() =>
        specialIconsTransform.gameObject.SetActive(true);

    public void SetColor(Color color) =>
        mainRoomImage.color = color;

    public void SetPosition(Vector2 position) =>
        GetComponent<RectTransform>().anchoredPosition = position;

    public void SetIconBoss(bool set) =>
        specialIconsTransform.Find("boss").gameObject.SetActive(set);

    public void SetIconReward(bool set) =>
        specialIconsTransform.Find("reward").gameObject.SetActive(set);

    public void SetOpening(int index)
    {
        //+1 because the mainRoomImage is first
        imagesTransform.GetChild(index + 1).gameObject.SetActive(true);
    }
}
