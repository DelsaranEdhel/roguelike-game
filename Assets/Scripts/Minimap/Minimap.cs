using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Minimap : MonoBehaviour
{
    private static Canvas minimapCanvas;

    [SerializeField]
    private GameObject minimapRoomPrefab;

    public static Action<Transform> createMinimapEvent;
    public static Action<Room> onRoomEnter;

    private MinimapRoom latestVisitedMinimapRoom;

    private const float minimapRoomOffsetX = 143f; //95.5f; //98.5f;
    private const float minimapRoomOffsetY = 68f; //45.5f; //48.5f;

    private Dictionary<Room, MinimapRoom> roomAndRoomMinimapPair;

    private void Awake()
    {
        minimapCanvas = GetComponentInParent<Canvas>();

        createMinimapEvent = null;
        onRoomEnter = null;

        createMinimapEvent -= GenerateMinimap;
        createMinimapEvent += GenerateMinimap;

        onRoomEnter -= OnRoomEnter;
        onRoomEnter += OnRoomEnter;
    }

    /// <summary>
    /// Generates a minimap by creating rooms and connections between them
    /// </summary>
    /// <param name="roomsTransform">Location where all rooms are</param>
    /// <exception cref="System.Exception"></exception>
    private void GenerateMinimap(Transform roomsTransform)
    {
        //make sure that there are no before created minimap rooms
        foreach (Transform child in transform)
            Destroy(child.gameObject);

        Room room;
        MinimapRoom minimapRoom;

        roomAndRoomMinimapPair = new Dictionary<Room, MinimapRoom>(roomsTransform.childCount);
        foreach(Transform roomTransform in roomsTransform)
        {
            //1. Create minimap room
            room = roomTransform.GetComponent<Room>();
            minimapRoom = Instantiate(minimapRoomPrefab, transform).GetComponent<MinimapRoom>();

            //2. Set position
            minimapRoom.SetPosition(new Vector2(room.gridPosition.x * minimapRoomOffsetX, room.gridPosition.y * minimapRoomOffsetY));

            //3. Set special icons
            minimapRoom.SetIconBoss(room is RoomBoss);
            minimapRoom.SetIconReward(room is RoomReward);

            //4. Make openings
            int roomOpeningChildIndex;
            foreach(Door door in room.DoorsInRoom)
            {
                roomOpeningChildIndex = door.DoorDirection switch
                {
                    Direction.UP => 0,
                    Direction.RIGHT => 1,
                    Direction.DOWN => 2,
                    Direction.LEFT => 3,
                    _ => throw new System.Exception("Unhandled Direction = " + door.DoorDirection),
                };

                minimapRoom.SetOpening(roomOpeningChildIndex);
            }

            roomAndRoomMinimapPair.Add(room, minimapRoom);
            minimapRoom.ShowRoom(false);
        }
    }

    /// <summary>
    /// Delegate's method which informs that a player has changed a room
    /// </summary>
    /// <param name="room">New room</param>
    private void OnRoomEnter(Room room)
    {
        if (latestVisitedMinimapRoom)
            latestVisitedMinimapRoom.SetColor(Color.white);

        RevealNeighboursIcons(room);

        latestVisitedMinimapRoom = roomAndRoomMinimapPair[room];
        latestVisitedMinimapRoom.ShowRoom(true);
        latestVisitedMinimapRoom.SetColor(new Color32(190, 255, 179, 255));

        GetComponent<RectTransform>().anchoredPosition = -latestVisitedMinimapRoom.GetComponent<RectTransform>().anchoredPosition;
    }

    private void RevealNeighboursIcons(Room room)
    {
        Vector2Int roomPos = room.gridPosition;
        Vector2Int difference;

        foreach (KeyValuePair<Room, MinimapRoom> entry in roomAndRoomMinimapPair)
        {
            difference = entry.Key.gridPosition - roomPos;

            if ((Mathf.Abs(difference.x) == 1 && difference.y == 0) || (Math.Abs(difference.y) == 1 && difference.x == 0))
                entry.Value.ShowIcons();
        }
    }

    public static void ShowMinimap()
    {
        bool enable = !minimapCanvas.enabled;

        minimapCanvas.enabled = enable;
        minimapCanvas.GetComponent<GraphicRaycaster>().enabled = enable;
    }
}
