public class RoomBoss : Room
{
    private RoomExit roomExitObject;

    protected override void Awake()
    {
        base.Awake();
        roomExitObject = GetComponentInChildren<RoomExit>();
        roomExitObject.gameObject.SetActive(false);
    }

    public override void InitializeRoom()
    {
        base.InitializeRoom();

        AudioManager.Instance.PlayBossMusic();
    }

    public override void DeleteEnemyFromRoom(CharacterEnemy characterEnemy)
    {
        base.DeleteEnemyFromRoom(characterEnemy);

        if (CanExit())
        {
            roomExitObject.gameObject.SetActive(true);
            AudioManager.Instance.PlayGameMusic();
        }
    }
}
