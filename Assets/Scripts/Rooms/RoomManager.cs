using System;
using UnityEngine;
using UnityEngine.UI;

public class RoomManager : MonoBehaviour
{
    private static RoomManager _instance;
    public static RoomManager Instance { get { return _instance; } }

    public Sprite openDoorSprite;
    public Sprite closeDoorSprite;
    [SerializeField]
    private Image transitionImage;

    public Action<CharacterEnemy> enemyKilled;
    private Room currentRoom;

    private void Awake()
    {
        //defining singleton
        if (Instance != null && Instance != this)
            Destroy(this.gameObject);
        else
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }

        enemyKilled += EnemyKilled;
    }

    private void Start()
    {
        //initialize starting room at the start of the game
        //StartLevel();

        Color32 color = transitionImage.color;
        color.a = 255;
        transitionImage.color = color;
    }

    public void StartLevel()
    {
        FindObjectOfType<RoomRandomizer>().GenerateLevel();
        currentRoom = GameObject.FindGameObjectWithTag("StartRoom").GetComponent<Room>();
        RoomChanged();
    }

    private void EnemyKilled(CharacterEnemy characterEnemy) =>
        currentRoom.DeleteEnemyFromRoom(characterEnemy);

    public void MoveToRoom(Room targetRoom, Direction entranceDirection)
    {
        FindObjectOfType<PlayerMovement>().SetPositionOnRoomEnter(targetRoom.GetExitPositionOfDoor(Door.GetOppositeDirection(entranceDirection)));

        currentRoom = targetRoom;
        RoomChanged();
    }

    private void RoomChanged()
    {
        //make transition (alpha of black background from 1 to 0)
        transitionImage.CrossFadeAlpha(1, 0f, false);
        transitionImage.CrossFadeAlpha(0, 0.5f, false);

        currentRoom.InitializeRoom();
        Vector3 cameraPos = currentRoom.transform.position - new Vector3(-0.5f, -0.6f, 0);
        cameraPos.z = -10;
        Camera.main.transform.position = cameraPos;

        Minimap.onRoomEnter?.Invoke(currentRoom);
    }
}
