using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    [SerializeField]
    private List<CharacterEnemy> enemiesInRoom = new List<CharacterEnemy>();
    public List<Door> DoorsInRoom { get; private set; }

    public Vector2Int gridPosition;

    protected virtual void Awake()
    {
        DoorsInRoom = new List<Door>();
    }

    public virtual void InitializeRoom()
    {
        ActivateEnemies();

        if (enemiesInRoom.Count > 0)
            CloseDoors();
        else
            OpenDoors();
    }

    public bool CanExit() => enemiesInRoom.Count <= 0;

    public void AssignEnemy(CharacterEnemy enemy) => enemiesInRoom.Add(enemy);

    /// <summary>
    /// Activate (enable) all enemies in the room
    /// </summary>
    protected virtual void ActivateEnemies()
    {
        foreach (CharacterEnemy enemy in enemiesInRoom)
            enemy.Activate();
    }

    public virtual void DeleteEnemyFromRoom(CharacterEnemy characterEnemy)
    {
        enemiesInRoom.Remove(characterEnemy);

        if (CanExit())
        {
            OpenDoors();
            ItemsRandomizer.RandomizeItemMultiple(this);
        }
    }

    public void AssignDoor(Door door) => DoorsInRoom.Add(door);

    private void OpenDoors()
    {
        foreach (Door door in DoorsInRoom)
            door.OpenDoor();
    }

    private void CloseDoors()
    {
        foreach (Door door in DoorsInRoom)
            door.CloseDoor();
    }

    public Vector3 GetExitPositionOfDoor(Direction direction)
    {
        foreach(Door door in DoorsInRoom)
        {
            if(door.DoorDirection.Equals(direction))
                return door.transform.position + (Vector3)door.GetExitOffset();
        }

        throw new System.Exception("No correct door was found!");
    }

    public Transform GetDoorsTransform() => transform.Find("doors");

    public int GetDoorsCount() => DoorsInRoom.Count;
}
