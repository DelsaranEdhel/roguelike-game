using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField]
    private Direction doorDirection;
    public Direction DoorDirection
    {
        get { return doorDirection; }
        private set { doorDirection = value; }
    }

    private bool isLocked;

    [SerializeField]
    private Room targetRoom;

    private SpriteRenderer spriteRenderer;

    //positions offsets when moving player to the room
    private Vector2 exitRightOffset = Vector2.left * 1.2f;
    private Vector2 exitLeftOffset = Vector2.right * 1.2f;
    private Vector2 exitUpOffset = Vector2.down * 0.5f;
    private Vector2 exitDownOffset = Vector2.up;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public void OpenDoor()
    {
        spriteRenderer.sprite = RoomManager.Instance.openDoorSprite;
        isLocked = false;
    }

    public void CloseDoor()
    {
        spriteRenderer.sprite = RoomManager.Instance.closeDoorSprite;
        isLocked = true;
    }

    public void GoThroughDoor()
    {
        if (isLocked || !targetRoom)
            return;

        RoomManager.Instance.MoveToRoom(targetRoom, DoorDirection);
    }

    public Vector2 GetExitOffset()
    {
        switch(DoorDirection)
        {
            case Direction.LEFT:
                return exitLeftOffset;
            case Direction.RIGHT:
                return exitRightOffset;
            case Direction.UP:
                return exitUpOffset;
            case Direction.DOWN:
                return exitDownOffset;
            default:
                throw new System.Exception("No proper direction was found!");
        }
    }

    public static Direction GetOppositeDirection(Direction dir)
    {
        switch(dir)
        {
            case Direction.LEFT:
                return Direction.RIGHT;
            case Direction.RIGHT:
                return Direction.LEFT;
            case Direction.UP:
                return Direction.DOWN;
            case Direction.DOWN:
                return Direction.UP;
            default:
                throw new System.Exception("No proper opposite direction was detected!");
        }
    }

    public void SetTargetRoom(Room room) => targetRoom = room;
}
