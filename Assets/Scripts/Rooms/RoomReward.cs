using UnityEngine;

public class RoomReward : Room
{
    [SerializeField]
    private Transform rewardSlotOne;
    [SerializeField]
    private Transform rewardSlotTwo;

    private Item itemOne;
    private Item itemTwo;

    protected override void Awake()
    {
        base.Awake();

        itemOne = ItemsRandomizer.RandomizeItemSingle(rewardSlotOne);
        itemTwo = ItemsRandomizer.RandomizeItemSingle(rewardSlotTwo);

        (itemOne as ItemBooster).OnItemBoosterPickUp += ItemBoosterPickUp;
        (itemTwo as ItemBooster).OnItemBoosterPickUp += ItemBoosterPickUp;
    }

    private void ItemBoosterPickUp()
    {
        rewardSlotOne.gameObject.SetActive(false);
        rewardSlotTwo.gameObject.SetActive(false);
    }
}
