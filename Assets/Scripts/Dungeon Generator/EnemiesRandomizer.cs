using System.Collections.Generic;
using UnityEngine;

public class EnemiesRandomizer : MonoBehaviour
{
    [SerializeField]
    private GameObject[] enemies;
    [SerializeField]
    private GameObject[] idleMovePointsGroups;
    private Vector3[] spawnPoints;

    [SerializeField]
    private GameObject[] bosses;

    [SerializeField]
    private Transform enemiesTransfom;

    private void Awake()
    {
        Transform spawnPointsTransform = transform.Find("SpawnPoints");
        spawnPoints = new Vector3[spawnPointsTransform.childCount];

        for (int i = 0; i < spawnPoints.Length; i++)
            spawnPoints[i] = spawnPointsTransform.GetChild(i).position;
    }

    public void RandomizeEnemies(Room room)
    {
        if (room is RoomBoss)
            CreateBoss(room);
        else
            CreateEnemies(room);
    }

    private void CreateBoss(Room room)
    {
        //spawn point (0,0) - middle of the room
        Vector3 spawnPoint = spawnPoints[0]; 

        GameObject enemy = Instantiate(bosses[0], enemiesTransfom);
        enemy.transform.position = spawnPoint + room.transform.position;
        enemy.SetActive(false);

        room.AssignEnemy(enemy.GetComponent<CharacterEnemy>());
    }

    private void CreateEnemies(Room room)
    {
        //1. Randomize the amount of enemies in the room
        float enemiesCountChance = Random.Range(0f, 1f);
        int enemiesCount = enemiesCountChance switch
        {
            float x when (x > 0 && x <= 0.05f) => 0,
            float x when (x > 0.05f && x <= 0.2f) => 1,
            float x when (x > 0.2f && x <= 0.4f) => 2,
            float x when (x > 0.4f && x <= 0.75f) => 3,
            float x when (x > 0.75f && x <= 0.9f) => 4,
            float x when (x > 0.9f && x < 1f) => 5,
            _ => 0,
        };

        Vector3 spawnPoint;
        GameObject enemy;
        //make list so we can remove already occupied spawn points
        List<Vector3> spawnPointsLeft = new List<Vector3>(spawnPoints);
        for (int i = 0; i < enemiesCount; i++)
        {
            if (spawnPointsLeft.Count <= 0)
                break;

            //2. Randomize spawn point
            spawnPoint = spawnPointsLeft[Random.Range(0, spawnPointsLeft.Count)];
            //3. Randomize type of the enemy
            enemy = Instantiate(enemies[Random.Range(0, enemies.Length)], enemiesTransfom);
            //4. Randomize and assign an idle move points group
            Instantiate(idleMovePointsGroups[Random.Range(0, idleMovePointsGroups.Length)], enemy.transform);

            enemy.transform.position = spawnPoint + room.transform.position;
            enemy.SetActive(false);

            room.AssignEnemy(enemy.GetComponent<CharacterEnemy>());

            spawnPointsLeft.Remove(spawnPoint);
        }
    }
}
