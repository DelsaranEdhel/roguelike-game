using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ItemsRandomizer : MonoBehaviour
{
    private const string itemsSinglePath = "Items/Single";
    private const string itemsMultiplePath = "Items/Multiple";

    private static List<GameObject> itemsOpen;
    private static List<GameObject> itemsMultiple;

    public static void Initialize()
    {
        itemsOpen = new List<GameObject>();
        itemsOpen.AddRange(Resources.LoadAll<GameObject>(itemsSinglePath).ToList());

        itemsMultiple = new List<GameObject>();
        itemsMultiple.AddRange(Resources.LoadAll<GameObject>(itemsMultiplePath).ToList());
    }

    public static Item RandomizeItemSingle(Transform parent)
    {
        if (itemsOpen == null)
            Initialize();

        int index = Random.Range(0, itemsOpen.Count);
        GameObject item = Instantiate(itemsOpen[index], parent);
        itemsOpen.RemoveAt(index);

        return item.GetComponent<Item>();
    }

    public static void RandomizeItemMultiple(Room room)
    {
        int itemIndex = Random.Range(0, itemsMultiple.Count);

        if (Random.Range(0f, 1f) >= 0.8f)
        {
            GameObject item = Instantiate(itemsMultiple[itemIndex]);
            item.transform.position = room.transform.position;
        }
    }
}
