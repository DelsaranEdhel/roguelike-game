using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RoomRandomizer : MonoBehaviour
{
    private EnemiesRandomizer enemiesRandomizer;
    private Transform roomsTransform;

    [SerializeField]
    private Room roomDefault;
    [SerializeField]
    private RoomBoss roomBoss;
    [SerializeField]
    private RoomReward roomReward;
    private Vector3 centerOffset = new Vector3(-0.5f, -0.6f, 0); //offset to center the room to the camera
    private const int offsetX = 20;
    private const int offsetY = 13;

    //0 - top, 1 - right, 2 - bottom, 3 - left
    [SerializeField]
    private Door[] doorsArray;

    private Queue<GridPositionInfo> finalGridPositions;
    private List<GridPositionInfo> endRooms;

    private int floorNumber = 1;

    private void Awake()
    {
        enemiesRandomizer = GetComponent<EnemiesRandomizer>();
        roomsTransform = transform.Find("Rooms");

        finalGridPositions = new Queue<GridPositionInfo>();
        endRooms = new List<GridPositionInfo>();
    }

    /// <summary>
    /// Generates a rooms for current level
    /// </summary>
    public void GenerateLevel()
    {
        //1. Randomize number of rooms that will be in the level
        int numberOfRooms = (int)(Random.Range(1, 3) + 5 + floorNumber * 2.6f);

        finalGridPositions.Clear();
        endRooms.Clear();

        //2. Create Queue that will save for what grid positions neighbours will be checked
        Queue<GridPositionInfo> gridPositionsToAnalyze = new Queue<GridPositionInfo>();
        gridPositionsToAnalyze.Enqueue(new GridPositionInfo(Vector2Int.zero, null));

        GridPositionInfo currentRoomGridPos;
        do
        {
            //3. End the algorithm if the number of rooms was reached
            if (finalGridPositions.Count >= numberOfRooms)
                break;

            currentRoomGridPos = gridPositionsToAnalyze.Dequeue();

            //4. If room is being analyzed, it means that it will be created, so remove parent of this room from end rooms (because parent will have more than one exit)
            endRooms.Add(currentRoomGridPos);
            endRooms.Remove(currentRoomGridPos.parentGridPosition);

            Vector2Int gridPosToCheck;

            //X = 1 to (1,0)
            //X = -1 to (-1,0)
            //Y = 1 to (0,1)
            //Y = -1 to (0,-1)

            //5. Check 2 neighbours (LEFT, RIGHT) for current grid position
            for (int x = 1; x >= -1; x -= 2)
            {
                gridPosToCheck = currentRoomGridPos.gridPosition + Vector2Int.right * x;

                //6. Randomize 50% chance to skip current direction
                if (Random.Range(0f, 1f) > 0.5f)
                    continue;

                //7. Check neighbours of neighbour grid position
                if (!finalGridPositions.Any(x => x.gridPosition == gridPosToCheck))
                {
                    Vector2Int tempPos;
                    bool neighbourHasNeighbour = false;
                    for (int xx = 1; xx >= -1; xx -= 2)
                    {
                        tempPos = gridPosToCheck + Vector2Int.right * xx;
                        if (tempPos != currentRoomGridPos.gridPosition && finalGridPositions.Any(x => x.gridPosition == tempPos))
                        {
                            neighbourHasNeighbour = true;
                            break;
                        }
                    }

                    for (int yy = 1; yy >= -1; yy -= 2)
                    {
                        tempPos = gridPosToCheck + Vector2Int.up * yy;
                        if (tempPos != currentRoomGridPos.gridPosition && finalGridPositions.Any(x => x.gridPosition == tempPos))
                        {
                            neighbourHasNeighbour = true;
                            break;
                        }
                    }

                    if (neighbourHasNeighbour)
                        continue;
                }
                else
                    continue;

                //8. If everything went alright then add the neighbour to analyze
                gridPositionsToAnalyze.Enqueue(new GridPositionInfo(gridPosToCheck, currentRoomGridPos));
            }

            //9. Check 2 neighbours (UP, DOWN) for current grid position
            for (int y = 1; y >= -1; y -= 2)
            {
                gridPosToCheck = currentRoomGridPos.gridPosition + Vector2Int.up * y;

                if (Random.Range(0f, 1f) > 0.5f)
                    continue;

                if (!finalGridPositions.Any(x => x.gridPosition == gridPosToCheck))
                {
                    Vector2Int tempPos;
                    bool neighbourHasNeighbour = false;
                    for (int xx = 1; xx >= -1; xx -= 2)
                    {
                        tempPos = gridPosToCheck + Vector2Int.right * xx;
                        if (tempPos != currentRoomGridPos.gridPosition && finalGridPositions.Any(x => x.gridPosition == tempPos))
                        {
                            neighbourHasNeighbour = true;
                            break;
                        }
                    }

                    for (int yy = 1; yy >= -1; yy -= 2)
                    {
                        tempPos = gridPosToCheck + Vector2Int.up * yy;
                        if (tempPos != currentRoomGridPos.gridPosition && finalGridPositions.Any(x => x.gridPosition == tempPos))
                        {
                            neighbourHasNeighbour = true;
                            break;
                        }
                    }

                    if (neighbourHasNeighbour)
                        continue;
                }
                else
                    continue;

                gridPositionsToAnalyze.Enqueue(new GridPositionInfo(gridPosToCheck, currentRoomGridPos));
            }

            //10. Add current room to final rooms
            finalGridPositions.Enqueue(currentRoomGridPos);

            //11. Make sure that there will be expected number of rooms
            if (gridPositionsToAnalyze.Count <= 0 && finalGridPositions.Count < numberOfRooms)
                gridPositionsToAnalyze.Enqueue(finalGridPositions.Dequeue());

        } while (gridPositionsToAnalyze.Count > 0);

        CreateRooms();
    }

    private Room CreateRoom(GameObject roomPrefab, Vector2Int gridPos)
    {
        GameObject room;
        room = Instantiate(roomPrefab, roomsTransform);
        room.transform.position = new Vector3(offsetX * gridPos.x + centerOffset.x, offsetY * gridPos.y + centerOffset.y, 0);

        Room roomComponent = room.GetComponent<Room>();
        roomComponent.gridPosition = gridPos;

        if (roomComponent.gridPosition.Equals(Vector2Int.zero))
            roomComponent.gameObject.tag = TagManager.startRoomTag;

        return roomComponent;
    }

    private void CreateRooms()
    {
        //1. Create special rooms
        //boos room
        GridPositionInfo gridPositionInfo = endRooms[Random.Range(0, endRooms.Count)];
        gridPositionInfo.AssignRoom(CreateRoom(roomBoss.gameObject, gridPositionInfo.gridPosition));

        endRooms.Remove(gridPositionInfo);
        if (endRooms.Count <= 0)
            throw new System.Exception("There is not enough end rooms to create a reward room");

        //reward room
        gridPositionInfo = endRooms[Random.Range(0, endRooms.Count)];
        gridPositionInfo.AssignRoom(CreateRoom(roomReward.gameObject, gridPositionInfo.gridPosition));
        endRooms.Remove(gridPositionInfo);

        //2. Create default rooms
        foreach (GridPositionInfo roomGridPos in finalGridPositions)
        {
            if (roomGridPos.GetRoom() != null)
                continue;

            roomGridPos.AssignRoom(CreateRoom(roomDefault.gameObject, roomGridPos.gridPosition));
        }

        //3. Create doors
        List<GridPositionInfo> tempList = finalGridPositions.ToList();

        //delete (0,0) grid position - this position has no parent
        GridPositionInfo zeroPos = tempList.Find(x => x.gridPosition.Equals(Vector2Int.zero));
        if(zeroPos != null)
            tempList.Remove(zeroPos);

        foreach (GridPositionInfo roomGridPos in finalGridPositions)
        {
            if (!tempList.Contains(roomGridPos))
                continue;

            Room currentRoom = roomGridPos.GetRoom();
            Room parentRoom = roomGridPos.parentGridPosition.GetRoom();
            Door currentDoor, parentDoor;

            Vector2Int neighbourPosDifference = roomGridPos.parentGridPosition.gridPosition - roomGridPos.gridPosition;
            switch (neighbourPosDifference)
            {
                case Vector2Int v when v.Equals(Vector2Int.up):
                    currentDoor = Instantiate(GetDoorInDirection(Direction.UP), currentRoom.GetDoorsTransform());
                    parentDoor = Instantiate(GetDoorInDirection(Direction.DOWN), parentRoom.GetDoorsTransform());
                    break;
                case Vector2Int v when v.Equals(Vector2Int.right):
                    currentDoor = Instantiate(GetDoorInDirection(Direction.RIGHT), currentRoom.GetDoorsTransform());
                    parentDoor = Instantiate(GetDoorInDirection(Direction.LEFT), parentRoom.GetDoorsTransform());
                    break;
                case Vector2Int v when v.Equals(Vector2Int.down):
                    currentDoor = Instantiate(GetDoorInDirection(Direction.DOWN), currentRoom.GetDoorsTransform());
                    parentDoor = Instantiate(GetDoorInDirection(Direction.UP), parentRoom.GetDoorsTransform());
                    break;
                case Vector2Int v when v.Equals(Vector2Int.left):
                    currentDoor = Instantiate(GetDoorInDirection(Direction.LEFT), currentRoom.GetDoorsTransform());
                    parentDoor = Instantiate(GetDoorInDirection(Direction.RIGHT), parentRoom.GetDoorsTransform());
                    break;
                default:
                    throw new System.Exception("Given difference between negihbour rooms is not correct: " + neighbourPosDifference);
            }

            currentDoor.SetTargetRoom(parentRoom);
            currentRoom.AssignDoor(currentDoor);

            parentDoor.SetTargetRoom(currentRoom);
            parentRoom.AssignDoor(parentDoor);

            tempList.Remove(roomGridPos);
        }

        CreateMinimap();
        CreateEnemies();
    }

    private void CreateMinimap() =>
        Minimap.createMinimapEvent?.Invoke(roomsTransform);

    private void CreateEnemies()
    {
        Room room;
        foreach(GridPositionInfo gridPosition in finalGridPositions)
        {
            room = gridPosition.GetRoom();

            //randomize enemies for each room (except start room and reward room)
            if (!room.CompareTag(TagManager.startRoomTag) && room is not RoomReward)
                enemiesRandomizer.RandomizeEnemies(room);
        }
    }

    private Door GetDoorInDirection(Direction direction)
    {
        return direction switch
        {
            Direction.UP => doorsArray[0],
            Direction.RIGHT => doorsArray[1],
            Direction.DOWN => doorsArray[2],
            Direction.LEFT => doorsArray[3],
            _ => throw new System.Exception("No condition for given Direction was found: " + direction),
        };
    }
}
