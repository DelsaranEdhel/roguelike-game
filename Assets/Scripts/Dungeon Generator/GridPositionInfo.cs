using UnityEngine;

public class GridPositionInfo
{
    public Vector2Int gridPosition;
    public GridPositionInfo parentGridPosition;
    private Room room;

    public GridPositionInfo(Vector2Int gridPosition, GridPositionInfo parentGridPosition)
    {
        this.gridPosition = gridPosition;
        this.parentGridPosition = parentGridPosition;
    }

    public void AssignRoom(Room room) => this.room = room;

    public Room GetRoom() => this.room;
}
