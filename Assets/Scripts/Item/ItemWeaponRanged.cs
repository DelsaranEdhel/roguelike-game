using UnityEngine;

public class ItemWeaponRanged : ItemWeapon
{
    protected ItemInfoWeaponRanged itemInfoWeaponRanged;

    protected override void Awake()
    {
        base.Awake();

        itemInfoWeaponRanged = base.itemInfo as ItemInfoWeaponRanged;
    }

    public override bool Attack()
    {
        if (!base.Attack())
            return false;

        WeaponManager.Instance.ShootArrow(itemInfoWeaponRanged, characterWhoPickedUp, characterWhoPickedUp.TargetAttackLayerMasks, transform.position, transform.right);

        return true;
    }

    public override bool WeaponMouseFollow()
    {
        if (!base.WeaponMouseFollow())
            return false;

        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0;
        transform.right = mousePos - transform.position;

        return true;
    }

    public override bool FaceWeaponTo(Vector3 pos)
    {
        if (!base.FaceWeaponTo(pos))
            return false;

        transform.right = pos - transform.position;

        return true;
    }

    public float GetArrowSpeed() =>
        itemInfoWeaponRanged.arrowSpeed;
}
