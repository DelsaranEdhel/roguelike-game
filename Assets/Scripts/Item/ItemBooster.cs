using System;
using UnityEngine;

public class ItemBooster : Item
{
    public Action OnItemBoosterPickUp;

    [HideInInspector]
    public ItemInfoBooster itemInfoBooster;

    protected override void Awake()
    {
        base.Awake();

        itemInfoBooster = itemInfo as ItemInfoBooster;
    }

    public override void PickUpItem(Character character)
    {
        base.PickUpItem(character);
        OnItemBoosterPickUp?.Invoke();
    }
}
