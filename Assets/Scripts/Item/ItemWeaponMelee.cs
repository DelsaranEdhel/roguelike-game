using UnityEngine;

public class ItemWeaponMelee : ItemWeapon
{
    protected ItemInfoWeaponMelee itemInfoWeaponMelee;

    [SerializeField]
    [Range(-10, 10)]
    protected float attackPointOffset;

    protected override void Awake()
    {
        base.Awake();

        itemInfoWeaponMelee = base.itemInfoWeapon as ItemInfoWeaponMelee;
    }

    public override bool Attack()
    {
        if (!base.Attack())
            return false;

        WeaponManager.Instance.MeleeSlash(itemInfoWeaponMelee, characterWhoPickedUp, transform.up + transform.position + (transform.up.normalized * attackPointOffset), characterWhoPickedUp.TargetAttackLayerMasks);

        return true;
    }

    public override bool WeaponMouseFollow()
    {
        if (!base.WeaponMouseFollow())
            return false;

        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0;
        transform.up = mousePos - transform.position;

        return true;
    }

    public override bool FaceWeaponTo(Vector3 pos)
    {
        if (!base.FaceWeaponTo(pos))
            return false;

        transform.up = pos - transform.position;

        return true;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.up + transform.position + (transform.up.normalized * attackPointOffset), (itemInfo as ItemInfoWeaponMelee).damageRadius);
    }
}
