using System.Collections;
using UnityEngine;

public abstract class ItemWeapon : Item
{
    private const float dropItemDuration = 1; //how long is item drop animation
    private const int weaponThrowDistanceModifier = 3; //how far the weapon will be thrown

    protected ItemInfoWeapon itemInfoWeapon;
    protected Animator animator;

    protected bool canAttack;
    protected bool hasAttackAnimEnded;
    protected AnimationClip attackAnimClip;

    protected AudioSource attackSound;

    protected override void Awake()
    {
        base.Awake();

        itemInfoWeapon = itemInfo as ItemInfoWeapon;
        animator = GetComponent<Animator>();
        attackSound = GetComponent<AudioSource>();

        //find and save the attack animation clip
        foreach (AnimationClip clip in animator.runtimeAnimatorController.animationClips)
        {
            if (clip.name == "attack_anim")
            {
                attackAnimClip = clip;
                break;
            }
        }

        canAttack = true;
        hasAttackAnimEnded = true;
    }

    public override void PickUpItem(Character character)
    {
        characterWhoPickedUp = character;
        pickUpCollider.enabled = false;
    }

    public void DropWeapon()
    {
        characterWhoPickedUp = null;
        transform.parent = null;
        StartCoroutine(DropWeaponCoroutine());
    }

    protected IEnumerator DropWeaponCoroutine()
    {
        Vector3 newDirection = Input.mousePosition;
        newDirection.z = 0;
        newDirection = Camera.main.ScreenToWorldPoint(newDirection);
        newDirection -= transform.position;

        float timeElapsed = 0;
        Vector3 targetPos = transform.position + (newDirection.normalized * weaponThrowDistanceModifier);
        while (timeElapsed < dropItemDuration)
        {
            transform.position = Vector3.Lerp(transform.position, targetPos, timeElapsed / dropItemDuration);
            timeElapsed += Time.deltaTime;
            yield return null;
        }

        pickUpCollider.enabled = true;
        transform.position = targetPos;

        //Debug.DrawRay(transform.position, newDirection, Color.red, 2f);
    }

    /// <summary>
    /// Rotates the weapon object to be always rotated to the mouse cursor
    /// </summary>
    /// <returns>Is weapon following mouse</returns>
    public virtual bool WeaponMouseFollow()
    {
        if (IsItemPickedUp() && !hasAttackAnimEnded)
            return false;

        return true;
    }

    /// <summary>
    /// Set weapon rotation to face a given pos (used by AI)
    /// </summary>
    /// <param name="pos">Target position to face at</param>
    /// <returns>Is weapon facing at player</returns>
    public virtual bool FaceWeaponTo(Vector3 pos)
    {
        if (IsItemPickedUp() && !hasAttackAnimEnded)
            return false;

        return true;
    }

    /// <summary>
    /// Performs an attack of a weapon
    /// </summary>
    /// <returns>Can attack be performed</returns>
    public virtual bool Attack()
    {
        if (!canAttack)
            return false;

        AttackSound();
        animator.SetTrigger("attack_trigger");
        StartCoroutine(AttackCooldown());

        return true;
    }

    protected virtual IEnumerator AttackCooldown()
    {
        canAttack = false;
        hasAttackAnimEnded = false;

        yield return new WaitForSeconds(attackAnimClip.length);
        hasAttackAnimEnded = true;

        yield return new WaitForSeconds(itemInfoWeapon.cooldownTime);
        canAttack = true;
    }

    protected virtual void AttackSound()
    {
        attackSound.Play();
    }
}
