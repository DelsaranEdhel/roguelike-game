using System;
using UnityEngine;

public class Item : MonoBehaviour
{
    public static Func<Item, bool> pickUpEvent;

    [SerializeField]
    protected ItemInfo itemInfo;

    protected SpriteRenderer spriteRenderer;
    protected BoxCollider2D pickUpCollider;

    protected Character characterWhoPickedUp = null;

    protected virtual void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        pickUpCollider = GetComponent<BoxCollider2D>();

        spriteRenderer.sprite = itemInfo.sprite;
    }

    protected bool IsItemPickedUp() => characterWhoPickedUp;

    public virtual void PickUpItem(Character character)
    {
        Destroy(this.gameObject);
    }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.TryGetComponent(out CharacterPlayer player))
        {
            //if item was picked up by character (true), then disable the trigger collider
            if ((bool)(pickUpEvent?.Invoke(this)))
                PickUpItem(player);
        }
    }
}
