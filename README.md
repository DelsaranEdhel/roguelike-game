# JustARoguelike
Unity and C#.

You can play this game [here](https://danioarcher.itch.io/just-a-roguelike?secret=yb4ZuIvb75r0ode6hmjY6xtWn4A).

A Roguelike game that uses a procedurally generated dungeons. Player can choose between two types of weapon - range or melee.
In the right upper corner there is a minimap. A room with a reward can be found, in this type of room you can collect some gear that increases the player's stats.
When the boss room was found and the boss was killed the game ends.

![image_1](Screenshots/image_1.png)
![image_2](Screenshots/image_2.png)
![image_3](Screenshots/image_3.png)
![image_4](Screenshots/image_4.png)